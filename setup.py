import setuptools

long_description="""
Handles the communication of Gentec Maestro energy meter via USB serial
with the help of pyserial.
"""

setuptools.setup(
    name="gentec_energy_meters",
    version="0.1.0",
    author="Vignesh Vaidyanathan",
    author_email="vignesh.vaidyanathan@hololinked.dev",
    description="Gentec energy meters with serial",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/hololinked-examples/gentec-optical-energy-meters",
    packages=['gentec_energy_meters'],
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires = [
        'hololinked',
        'pyserial',
    ],
    python_requires='>=3',
)
 