# Gentec Optical Energy Meters

Device server for Gentec Optical Energy Meters, especially Gentec Maestro and Gentec ULink. Supports HTTP, ZMQ based TCP/inter-process communication. 

Edit code to suit your requirements. 

Two branches
- simple : single computer application - demonstrates how to make such a device server in a lab or a small setup:
  - start from `gentec_energy_meters/base_device.py` & `gentec_energy_meters/gentec.py` - implements the devices
  - then `examples/scripting.py` - object instance level scripting without client-server model
  - `examples/server.py` & `examples/client.py` - scripting with client-server separation 
  - `gentec_energy_meters/extensions.py` - pushing events
  - `examples/pyqt_gui` - qt GUI example with client-server separation but without network expose of device
  - `examples/panel` - holoviz Panel with client-server separation, only Panel GUI can be exposed to the network without the device object itself. In this way your device can be protected without credentials while exposing a subset of device functionalities to an user. 
  - `examples/http_server` - HTTP server which can also expose the full device to the network
  
- sophisticated : full fledged IoT/Network device (Still under development - nothing works).

pip installable. 

Depends on 
- `hololinked`
- serial utility : https://gitlab.com/hololinked-examples/serial-utility
- pyserial 
- device drivers of Gentec 

You can scrap that serial utility and make your own as well, which may be simpler 

Please [write to me](https://hololinked.dev/contact) if you need any features or demonstrations.

#### Code Definitions

`gentec_energy_meters.BaseMeter` - base class for both Gentec Maestro and ULink devices (in future may also for others) <br/>
`gentec_energy_meters.Maestro` - controls Gentec Maestro <br/>
`gentec_energy_meters.ULink` - controls Gentec U-Link <br/>
`gentec_energy_meters.sensor` - base class for implementing a sensor if sensor information is necessary <br/>
`gentec_energy_meters.allowed_sensors` - editable dictionary containing allowed sensors to be used by the energy meter. Add your own if needed <br/>
`gentec_energy_meters.QE25LP_S_MB` - sensor QE25LP_S_MB <br/>
`gentec_energy_meters.QE12LP_S_MB_QED_D0`- sensor QE12LP_S_MB_QED_D0

###### examples folder 
demostrates how to use. <br/>

## Installation
`pip install -e .` or `pip install .` after setting working directory of the terminal to the directory of the repository.  <br/>

