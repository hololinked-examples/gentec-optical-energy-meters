import sys
import os
current_directory = os.path.dirname(os.path.abspath(__file__))
parent_directory = os.path.abspath(os.path.join(current_directory, os.pardir))
sys.path.append(parent_directory)
from gentec_energy_meters import Maestro


if __name__ == "__main__":
    # Puts the object/device on the network through HTTP
    Maestro(
        instance_name='gentec-maestro', 
        serial_url='COM4'
    ).run_with_http_server(port=9395)


