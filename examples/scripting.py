import sys
import os
current_directory = os.path.dirname(os.path.abspath(__file__))
parent_directory = os.path.abspath(os.path.join(current_directory, os.pardir))
sys.path.append(parent_directory)
from gentec_energy_meters import Maestro
from time import sleep


if __name__ == "__main__":
    dev = Maestro(instance_name='gentec-maestro', serial_url='COM4')

    # Executing direct serial command
    print("Direct serial command reply : " , dev.serial_comm_handle.execute_instruction("*VER\r\n", 50))

    # ranges
    print("autorange : ", dev.autorange)
    print("range : ", dev.range)

    # Get display mode of the meter
    print("Display mode : " , dev.display_mode)

    # Sensor details
    print("Sensor name : ", dev._attached_sensor.name)
    print("Sensor details : ", dev._attached_sensor.specifications)

    # Min and Max Allowed wavelengtj
    print("min (software limit) wavelength : ", dev.min_wavelength)
    print("max (software limit) wavelength : ", dev.max_wavelength)

    # Set other min and max wavelength 
    dev.min_wavelength = 250
    dev.max_wavelength = 1500
    try: 
        dev.max_wavelength = "ABC"
    except TypeError:
        print("Cant set ABC to max wavelength")

    try: 
        dev.max_wavelength = 1500.0
    except TypeError:
        print("Cant set float to max wavelength as its an integer")

    #Min and Max offset
    print("min (software) offset : ", dev.min_offset)
    print("max (software) offset : ", dev.max_offset)

    # Set other min and max offset
    dev.min_offset = 0
    dev.max_offset = 100

    # Trigger Level
    print("Currently set trigger level : ", dev.trigger_level)
    dev.trigger_level = 50 
    print("Newly set trigger level : ", dev.trigger_level)
    # Trigger level exception
    try: 
        dev.trigger_level = -1 
    except ValueError:
        print("cannot set negative trigger level")

    print("Newly set trigger level : ", dev.trigger_level)
    # Current Value Measurement
    for i in range(5):
        if dev.new_value_ready:
            print("new value available")
            print("energy : " , dev.current_value)  
        else:
            print("no new value available")
        sleep(0.5)

    # Laser Frequency
    print("Laser Pulse Frequency : " , dev.pulse_frequency)

    # wavelength
    print("Current measurement wavelength : ", dev.wavelength)
    dev.wavelength = 800
    print("New measurement wavelength : ", dev.wavelength)
    # wavelength exception
    try:
        dev.wavelength = 2150
    except ValueError:
        print("too high")
    
    # Offset
    print("Current Offset : ", dev.offset)
    print("Zero Offset Active  : ", dev.zero_offset_active)
    dev.offset = 10
    print("Current Offset : ", dev.offset)
    print("Zero Offset Active  : ", dev.zero_offset_active)

    # Offset exception
    try: 
        dev.offset = -1
    except ValueError:
        print("Cant set negative offset")

    #multiplier
    print("Current multiplier : ", dev.multiplier)
    dev.multiplier = 2
    print("Current multiplier : ", dev.multiplier)

    # Current Value Measurement with multiplier and offset
    for i in range(10):
        print("new value available : " , dev.new_value_ready)  
        print("energy : " , dev.current_value)
        if i == 3:
            print("Zero Offset Active  : ", dev.zero_offset_active)
            dev.set_current_value_as_zero_offset()
            print("Zero Offset Active  : ", dev.zero_offset_active)
        if i == 6:
            dev.clear_zero_offset()
        if i == 8:
            dev.clear_multiplier()
        sleep(0.5)

    # test connection
    print("Device active : ", dev.ping_instrument())

    # Version 
    print("Device : ", dev.version)

    # String conversion examples
    print(dev.get_number_as_instruction(200))
    print(dev.get_number_as_instruction(200.0))
    print(dev.get_number_as_instruction(-200))
    print(dev.get_number_as_instruction(-200.0))
    print(dev.get_number_as_instruction(1e-25))
    print(dev.get_number_as_instruction(-1e-25))
    print(dev.get_number_as_instruction(1.0e-25))
    print(dev.get_number_as_instruction(-1.0e-25))
    print(dev.get_number_as_instruction(-1.0e-2))
    print(dev.get_number_as_instruction(2000.000))

    dev.serial_comm_handle.disconnect()
    print("DONE")