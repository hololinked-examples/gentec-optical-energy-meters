import sys
import os, logging
current_directory = os.path.dirname(os.path.abspath(__file__))
parent_directory = os.path.abspath(os.path.join(current_directory, os.pardir))
sys.path.append(parent_directory)
from gentec_energy_meters import Maestro


if __name__ == "__main__":
    # Does NOT expose object/device to the network, 
    # only locally available within this computer
    # IPC is ZMQ's interprocess communication transport method
    Maestro(
        instance_name='gentec-maestro', 
        serial_url='COM4',
        log_level=logging.DEBUG
    ).run(zmq_protocols='IPC')
