### Example of a Panel GUI

Using such web apps to communicate with devices through IPC, you can restrict network access through GUIs 
instead of directly giving network access to devices. 

![Alt Text](panel_example.gif)