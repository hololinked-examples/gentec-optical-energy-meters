import logging, multiprocessing, asyncio
import panel as pn
import plotly.graph_objs as go

from collections import deque
from gentec_energy_meters.extensions import GentecMaestroEnergyMeter
from hololinked.client import ObjectProxy

asyncio.set_event_loop_policy(asyncio.WindowsSelectorEventLoopPolicy())


print("Panel app starting")
def start_server():
    import logging, sys, os

    current_directory = os.path.dirname(os.path.abspath(__file__))
    parent_directory = os.path.abspath(os.path.join(current_directory, os.pardir))
    sys.path.append(parent_directory)

    GentecMaestroEnergyMeter(
        instance_name='gentec-maestro',
        serial_url='COM6',
        log_level=logging.DEBUG # you can also set log level
    ).run(zmq_protocols='IPC')


# if __name__ == '__main__':
#     uncomment this to also start the server within the same script
#     using the above function. Be careful of the panel code that follows
#     as its not under this tab 
#     device_process = multiprocessing.Process(target=start_server)
#     device_process.start()

# client 

client = None # type: GentecMaestroEnergyMeter

def connect_client():
    global client 
    client = ObjectProxy('gentec-maestro', zmq_protocols='IPC', 
                        log_level=logging.DEBUG, async_mixin=True) 

connect_client()


# components

settings_styles = {'font-size': '16pt', 'padding-top': '10px'}



async def set_trigger_level(event):
    client.trigger_level = trigger_level.value # set to server
    await asyncio.sleep(0.3) 
    # immediate read somehow causes some serial port error, not clear why.
    # so we wait a bit on client side (0.3 seconds). This is a server side error
    # and should be solved on server side, not here in general.
    trigger_level.value = client.trigger_level 
    # read value can differ from written value - so we read it and set it back
    # to the widget to make sure the widget is in sync with the server

trigger_level = pn.widgets.FloatSlider(
    name="Trigger Level", 
    start=GentecMaestroEnergyMeter.trigger_level.bounds[0],
    end=GentecMaestroEnergyMeter.trigger_level.bounds[1], 
    value=client.trigger_level or GentecMaestroEnergyMeter.trigger_level.default or 0.1, 
    # initial value, first try fetching from server, if not load parameter's default or a hardcoded default
    step=GentecMaestroEnergyMeter.trigger_level.step,
    styles=settings_styles,
) # technically it would be nice to bind this component directly to the server parameter

trigger_level.param.watch(set_trigger_level, 'value_throttled')


# Autorange the measurement range of the device 

async def set_autorange(event):
    client.autorange = autorange.value
    await asyncio.sleep(0.3) 
    autorange.value = client.autorange

autorange = pn.widgets.Checkbox(
    name='Auto Range', 
    styles=settings_styles,
    value=client.autorange or GentecMaestroEnergyMeter.autorange.default or False
)

autorange.param.watch(set_autorange, 'value')


# Set the wavelength of the measurement

async def set_wavelength(event):
    client.wavelength = wavelength.value
    await asyncio.sleep(0.3) 
    wavelength.value = client.wavelength

wavelength = pn.widgets.IntInput(
    name='Wavelength',
    styles=settings_styles,
    value=client.wavelength or GentecMaestroEnergyMeter.wavelength.default or 0 # 
)

wavelength.param.watch(set_wavelength, 'value_throttled')


# Set the offset and multiplier of the measurement

async def set_offset(event):
    # all async example
    await client.async_set_property('offset', offset.value)
    await asyncio.sleep(0.3) 
    offset.value = await client.async_get_property('offset')

offset = pn.widgets.FloatInput(
    name='Offset',
    styles=settings_styles,
    value=client.offset or GentecMaestroEnergyMeter.offset.default or 0.0
)

offset.param.watch(set_offset, 'value_throttled')


# multiplier of the measured value

async def set_multiplier(event):
    client.multiplier = multiplier.value
    await asyncio.sleep(0.3) 
    multiplier.value = client.multiplier

multiplier = pn.widgets.FloatInput(
    name='Multiplier',
    styles=settings_styles,
    value=client.multiplier or GentecMaestroEnergyMeter.multiplier.default or 1.0
)

multiplier.param.watch(set_multiplier, 'value_throttled')


# analog output pin

async def set_analog_output(event):
    if analog_output.value:
        client.enable_analog_output()
    else:
        client.disable_analog_output() 
    analog_output.value = client.analog_output_enabled
    
analog_output = pn.widgets.Checkbox(
    name='Analog Output',
    styles=settings_styles,
    value=client.analog_output_enabled or GentecMaestroEnergyMeter.analog_output_enabled.default or False
)

analog_output.param.watch(set_analog_output, 'value')


# Graph and indicators
pn.extension("plotly")
pn.extension(sizing_mode="stretch_width")

energy = deque(maxlen=300)
timestamp = deque(maxlen=300)

fig = go.Figure(
    data=[
        go.Scatter(
            x=list(timestamp),
            y=list(energy),
            mode="lines+markers", 
            marker=dict(size=5), 
            line=dict(width=4)
        )
    ],
    layout=dict(
        autosize=True,
        title="Pulse Energy vs Time",
        xaxis=dict(
            title='Time (HH:MM:SS.fff)',
            tickangle=-45,  # rotate the tick labels by 45 degrees
            tickfont=dict(size=10),  # adjust the font size of the tick labels
            nticks=25
        ),
        yaxis=dict(
            title='Energy (J)',
            autorange=True
        )
    )
)

graph = pn.pane.Plotly(fig, sizing_mode='scale_both')
energy_indicator = pn.indicators.Number(name='Energy', value=0.0, format='{value}J')
last_timestamp_indicator = pn.pane.Str('Last Timestamp', styles={'font-size': '16pt'})
statistics_viewer = pn.pane.Markdown('Statistics will be displayed here if enabled', 
                                    sizing_mode='stretch_width')

def energy_measurement_callback(event_data):
    energy_indicator.value = event_data["energy"]
    last_timestamp_indicator.object = f"Last Timestamp : {event_data['timestamp']}"
    energy.append(event_data["energy"])
    timestamp.append(event_data["timestamp"])
    fig.data[0].x = list(timestamp)
    fig.data[0].y = list(energy)

client.subscribe_event('data-point-event', energy_measurement_callback)
# the bokeh app hangs because the event subscription thread is still running.
# force quit app for now 


# acquisition controls

start_acquisition_button = pn.widgets.Button(
    name='Start', 
    button_type='primary',
    description='Start Energy Acquisition',
)

pn.bind(
    lambda _ : client.start_acquisition(), 
    start_acquisition_button,
    watch=True # mandatory for the button to invoke the function
)

stop_acquisition_button = pn.widgets.Button(
    name='Stop', 
    button_type='primary',
    description='Stop Energy Acquisition',
)

pn.bind(
    lambda _ : client.stop_acquisition(), 
    stop_acquisition_button,
    watch=True # mandatory for the button to invoke the function
)



def set_data_point_history(event):
    global energy, timestamp
    energy = deque(energy, maxlen=data_point_history.value)
    timestamp = deque(timestamp, maxlen=data_point_history.value)

data_point_history = pn.widgets.IntSlider(
    name='Data Point History',
    start=100,
    end=5000,
    value=energy.maxlen,
    step=10,
    sizing_mode='stretch_width'
)

data_point_history.param.watch(set_data_point_history, 'value_throttled')

# statistics 

statistics = pn.widgets.Checkbox(
    name='Statistics',
    styles=settings_styles,
    value=False
)

def set_statistics(event):
    if statistics.value:
        client.enable_statistics()
    else:
        client.disable_statistics()
    statistics.value = client.statistics_enabled

statistics.param.watch(set_statistics, 'value')

def statistics_callback(event_data):
    statistics_viewer.object = f"```json\n{event_data}\n```"

client.subscribe_event('statistics-event', statistics_callback)

# gap between measurements

async def set_measurement_gap(event):
    client.measurement_gap = measurement_gap.value
    await asyncio.sleep(0.3) 
    measurement_gap.value = client.measurement_gap

measurement_gap = pn.widgets.FloatInput(
    name='Measurement gap in seconds',
    styles=settings_styles,
    step=0.01, start=0,
    value=client.measurement_gap or GentecMaestroEnergyMeter.measurement_gap.default or 0.1
)

measurement_gap.param.watch(set_measurement_gap, 'value_throttled')


# cleanup function to clean up event threads in client object

def cleanup(event):
    client.stop_acquisition()
    client.unsubscribe_event('data-point-event')
    client.unsubscribe_event('statistics-event')

cleanup_button = pn.widgets.Button(
    name='Cleanup to Quit Bokeh Server', 
    button_type='danger',
    description='unsubscribes from server events so that there are no lingering threads which prevent quitting',
)

pn.bind(cleanup, cleanup_button, watch=True)

print("Panel app is ready to be served")
pn.template.MaterialTemplate(
    title="Gentec Maestro Energy Meter", 
    main=[pn.Column(graph, 
            pn.Row(
                pn.Column(energy_indicator, last_timestamp_indicator),
                statistics_viewer
            )
        )],
    sidebar=[
            pn.pane.Str("Settings", styles={'font-size': '20pt'}),
            trigger_level, autorange, wavelength, offset, multiplier, analog_output,
            pn.layout.Divider(),
            pn.pane.Str("Acquisition Controls", styles={'font-size': '20pt'}),
            pn.Row(start_acquisition_button, stop_acquisition_button),
            data_point_history, measurement_gap, statistics,
            pn.layout.Divider(),
            cleanup_button
        ],
).servable()