import sys
import os
current_directory = os.path.dirname(os.path.abspath(__file__))
parent_directory = os.path.abspath(os.path.join(current_directory, os.pardir))
sys.path.append(parent_directory)

from hololinked.client import ObjectProxy
from gentec_energy_meters import Maestro # This import is optional, but it will help you with code editor suggestions or type definitions
from time import sleep

# Run server.py first before running this script

if __name__ == "__main__":
    dev_proxy = ObjectProxy(
                    instance_name='gentec-maestro', 
                    zmq_protocols='IPC' # zmq protocol set in server.py
                ) # type: Maestro
    # add 'type: Maestro' to the end of the line to get code editor suggestions or type definitions
    # the client does not know the type of the server object, so it will not give you suggestions or type definitions by default

    # ranges
    print("autorange : ", dev_proxy.autorange) 
    print("range : ", dev_proxy.range)
    # these are the properties of the server object, remember,
    # the client makes the request to read this property from the server object in this statement
    # One would be doing this, for example, in a PyQt or Panel GUI to populate a value in the GUI

    # Get display mode of the meter
    print("Display mode : " , dev_proxy.display_mode)

    # Min and Max Allowed wavelength
    print("min (software limit) wavelength : ", dev_proxy.min_wavelength)
    print("max (software limit) wavelength : ", dev_proxy.max_wavelength)

    # Example of writing properties
    dev_proxy.min_wavelength = 250
    dev_proxy.max_wavelength = 1500
    try: 
        dev_proxy.max_wavelength = "ABC"
    except TypeError:
        print("Cant set ABC to max wavelength")

    try: 
        dev_proxy.max_wavelength = 1500.0
    except TypeError:
        print("Cant set float to max wavelength as its an integer")

    #Min and Max offset
    print("min (software) offset : ", dev_proxy.min_offset)
    print("max (software) offset : ", dev_proxy.max_offset)

    # Set other min and max offset
    dev_proxy.min_offset = 0
    dev_proxy.max_offset = 100

    # Trigger Level
    print("Currently set trigger level : ", dev_proxy.trigger_level)
    dev_proxy.trigger_level = 50 
    print("Newly set trigger level : ", dev_proxy.trigger_level)
    # Trigger level exception
    try: 
        dev_proxy.trigger_level = -1 
    except ValueError:
        print("cannot set negative trigger level")

    print("Newly set trigger level : ", dev_proxy.trigger_level)
    # Current Value Measurement
    for i in range(5):
        if dev_proxy.new_value_ready:
            print("new value available")
            print("energy : " , dev_proxy.current_value)  
        else:
            print("no new value available")
        sleep(0.5)

    # Laser Frequency
    print("Laser Pulse Frequency : " , dev_proxy.pulse_frequency)

    # wavelength
    print("Current measurement wavelength : ", dev_proxy.wavelength)
    dev_proxy.wavelength = 800
    print("New measurement wavelength : ", dev_proxy.wavelength)
    # wavelength exception
    try:
        dev_proxy.wavelength = 2150
    except ValueError:
        print("too high")
    
    # Offset
    print("Current Offset : ", dev_proxy.offset)
    print("Zero Offset Active  : ", dev_proxy.zero_offset_active)
    dev_proxy.offset = 10
    print("Current Offset : ", dev_proxy.offset)
    print("Zero Offset Active  : ", dev_proxy.zero_offset_active)

    # Offset exception
    try: 
        dev_proxy.offset = -1
    except ValueError:
        print("Cant set negative offset")

    #multiplier
    print("Current multiplier : ", dev_proxy.multiplier)
    dev_proxy.multiplier = 2
    print("Current multiplier : ", dev_proxy.multiplier)

    # Current Value Measurement with multiplier and offset
    for i in range(10):
        print("new value available : " , dev_proxy.new_value_ready)  
        print("energy : " , dev_proxy.current_value)
        # Examples of calling actions
        if i == 3:
            print("Zero Offset Active  : ", dev_proxy.zero_offset_active)
            dev_proxy.set_current_value_as_zero_offset()
            print("Zero Offset Active  : ", dev_proxy.zero_offset_active)
        if i == 6:
            dev_proxy.clear_zero_offset()
        if i == 8:
            dev_proxy.clear_multiplier()
        sleep(0.5)

    # test connection
    print("Device active : ", dev_proxy.ping_instrument())

    # Version 
    print("Device : ", dev_proxy.version)

    print("DONE")

    # Events examples will be separately dealt