import logging
import sys
import os
import datetime
import threading
import ssl
import time
from multiprocessing import Process
from collections import deque
from dataclasses import dataclass

current_directory = os.path.dirname(os.path.abspath(__file__))
parent_directory = os.path.abspath(os.path.join(current_directory, os.pardir))
sys.path.append(parent_directory)

from hololinked.server import Event, action, Property, HTTPServer
from hololinked.server.properties import Number	
from gentec_energy_meters import Maestro


@dataclass 
class EnergyHistory:
    """A history of energy data points along with the timsestamp of measurement""" 
    timestamp : deque 
    energy : deque

    def json(self):
        return {
            'timestamp' : list(self.timestamp) if len(self) > 0 else None,
            'energy' : list(self.energy) if len(self) > 0 else None
        }
    
    def __len__(self):
        assert len(self.timestamp) == len(self.energy), "unequal length of timestamp and energy data detected"
        return len(self.timestamp)
    
@dataclass
class EnergyDataPoint:
    """A single data point of energy measurement along with the timestamp of measurement"""
    timestamp : str
    energy : float

    def json(self):
        return {
            'timestamp' : self.timestamp,
            'energy' : self.energy
        }



class GentecMaestroEnergyMeter(Maestro):
    """
    Simple example to implement acquisition loops and events
    to push the captured data. Customize it for your application or 
    implement your own.
    """

    data_point_event = Event(friendly_name='data-point-event', 
                            doc='Event raised when a new data point is available',
                            label='Data Point Event')
    
    statistics_event = Event(friendly_name='statistics-event',
                            doc='Event raised when a new statistics is available',
                            label='Statistics Event')

    energy_history = Property(default=EnergyHistory(timestamp=deque(maxlen=300), energy=deque(maxlen=300)), 
                    readonly = True,
                    doc="Energy data as timestamps (datetime) in x-axis and energy value in y-axis (mJ)") # type: EnergyHistory

    measurement_gap = Number(default=0.1, bounds=(0, None), allow_None=True, 
                        doc="Time gap between two measurements, unit - seconds.")
    
    def __init__(self, instance_name, serial_url = "COM4", **kwargs):
        super().__init__(instance_name, serial_url, **kwargs)
        self._run = False
        self._measurement_worker = None
        self._last_measurement = None

    def loop(self):
        self._run = True
        while self._run:
            # Since the data point is a single float value and a timestamp which are very very small in size,
            # Its also sufficient if one implement's this loop purely at a client level. 
            # The server implementation is useful when the acquisition has to happen indenpendently of the
            # client.
            if self.new_value_ready:
                self._last_measurement = self.current_value
                timestamp = datetime.datetime.now()
                timestamp = timestamp.strftime("%H:%M:%S") + '.{:03d}'.format(int(timestamp.microsecond /1000))
                self.energy_history.timestamp.append(timestamp)
                self.energy_history.energy.append(self._last_measurement)
                self.data_point_event.push(EnergyDataPoint(timestamp=timestamp, energy=self._last_measurement))
                if self._statistics_enabled:
                    self.statistics_event.push(self.statistics)
                self.logger.debug(f"New data point : {self._last_measurement} J")
            else:
                self.logger.debug("No new data point available")
            # auto serialization of the event data happens when a json() method is implemented,
            # which has been done in the dataclass
            time.sleep(self.measurement_gap)


    @action()
    def start_acquisition(self):
        """Start the acquisition loop"""
        if not self._run: 
            self._measurement_worker = threading.Thread(target=self.loop)
            self._measurement_worker.start()
            self.logger.info("Acquisition loop started")
        else: 
            self.logger.info("Acquisition loop already running")

    @action()
    def stop_acquisition(self):
        """Stop the acquisition loop"""
        if self._run:
            self._run = False
            self._measurement_worker.join()
            self._measurement_worker = None
            self.logger.info("Acquisition loop stopped")
        else:
            self.logger.info("Acquisition loop already stopped")


def start_https_server():
    ssl_context = ssl.SSLContext(protocol = ssl.PROTOCOL_TLS_SERVER)
    ssl_context.load_cert_chain(f'assets{os.sep}security{os.sep}certificate.pem',
                        keyfile=f'assets{os.sep}security{os.sep}key.pem')
    ssl_context.minimum_version = ssl.TLSVersion.TLSv1_3

    H = HTTPServer(things=['gentec-maestro'], 
                    port=8085, log_level=logging.DEBUG,  ssl_context=ssl_context)
    H.listen()


if __name__ == '__main__':
    P = Process(target=start_https_server)
    P.start()
    # Uncomment above for HTTPS server simulatenously

    # A server for a PC-local client
    GentecMaestroEnergyMeter(
        instance_name='gentec-maestro',
        serial_url='COM4',
        log_level=logging.DEBUG # you can also set log level
    ).run(zmq_protocols='IPC')