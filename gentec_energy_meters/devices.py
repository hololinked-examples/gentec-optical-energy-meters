from hololinked.server import Property, action
from hololinked.server.properties import Boolean
from .base_device import BaseMeter


class Maestro(BaseMeter):
    """
    Control Gentec Maestro optical energy meters through serial interface using this class. 
    """

    statistics = Property(doc="Get latest computed statistics", 
                        readonly=True)
    
    statistics_enabled = Boolean(doc="Check if statistics is enabled", 
                                readonly=True, fget=lambda self: self._statistics_enabled)
    
    def __init__(self, instance_name, serial_url, **kwargs):
        super().__init__(instance_name, serial_url, **kwargs)
        self._statistics_enabled = False
    
    @statistics.getter
    def get_statistics(self):
        """get latest computed statistics"""
        raw_data = self.serial_comm_handle.execute_instruction("*VSU\r\n", 1000)
        if raw_data is not None:
            data = raw_data.split('\t')
            ret = {}
            for qty in data:
                name, value = qty.split(':')
                ret[name] = float(value)
            return ret
        raise RuntimeError("Could not get statistics")
    
    @action()
    def enable_statistics(self) -> None:
        """enable statistics calculation"""
        self.serial_comm_handle.execute_instruction("*ESU1\r\n")
        self._statistics_enabled = True 
        # There are better ways to find if statistics is enabled than in this way,
        # but this is just an example (including the __init__).

    @action()
    def disable_statistics(self) -> None:
        """disbale statistics calculation"""
        self.serial_comm_handle.execute_instruction("*ESU0\r\n")
        self._statistics_enabled = False



class ULink(BaseMeter):
    """
    Control Gentec ULink optical energy meters through serial interface using this class. 
    """
    pass 

