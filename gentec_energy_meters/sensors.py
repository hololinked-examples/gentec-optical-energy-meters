import typing

from hololinked.server import Thing
from hololinked.server.properties import String, TypedDict


class Sensor(Thing):
    """
    Base class for a Gentec Maestro sensor. subclass it to add features.
    """
    name = ""
    specifications = {}

    def configure_gentec_meter(self, meter_instance : typing.Any) -> None: 
        """
        Here you can include functionality to manipulate variables of the Gentec instance 
        according to a certain sensor, like minimum and maximum allowed wavelength.
        """
        meter_instance.min_wavelength = int(self.specifications.get("Minimum Wavelength", '0nm').strip("nm"))
        meter_instance.max_wavelength = int(self.specifications.get("Maximum Wavelength", '{}nm'.format(pow(2,31))).strip("nm"))
     

class QE25LP_S_MB(Sensor):
    

    name = String(default="QE25LP-S-MB", readonly=True, 
                doc="The name of the sensor.", class_member=True)
    
    specifications = TypedDict(default={
                                    "Name"               : "QE25LP-S-MB",
                                    "Absorber"           : "MB",
                                    "Minimum Wavelength" : "248nm",
                                    "Maximum Wavelength" : "2100nm",
                                    "Total Spectral Range (inkl. Uncalibrated)" : "190 - 20000 nm",
                                    "Typical Sensitivity"     : "10 V/J",
                                    "Calibration Uncertainty" : "+- 3%",
                                    "Repeatability"           : "<0.5%",   
                                    "Max Pulse Energy 1064nm" : "3.75J",
                                    "Max Pulse Energy 266nm"  : "3.1J",
                                    "Noise Equivalent Energy" : "4uJ",
                                    "Max Repetition Rate"     : "300Hz",
                                    "Typical Rise Time"       : "550µsec",
                                    "Max Pulse Width"         : "400µsec",
                                    "Max Energy Density 1064nm" : "600mJ/cm^2 , 7ns, 10Hz",
                                    "Max Energy Density 266nm"  : "500mJ/cm^2 , 7ns, 10Hz",
                                    "Max Average Power w/o Heatsink"  : "5W",
                                    "Max Average Power with Heatsink" : "10W",
                                    "Max Power Density"         : "10W/cm^2 @ 5W",
                                    "Dimensions w/o Heatsink"   : "50x50x14mm",
                                    "Dimensions with Heatsink"  : "50x50x52.5mm",
                                    "Weight w/o Heatsink"       : "120g",
                                    "Weight with Heatsink"      : "187g",
                                    "Aperture"                  : "25x25mm",
                                    "Aperture Area"             : "6.25cm^2"
                                    },  readonly=True, class_member=True,
                                    doc="The specifications of the sensor.")

    
    
class QE12LP_S_MB_QED_D0(Sensor):
    
    name = String(default="QE12LP-S-MB-QED-D0", readonly=True, 
                doc="The name of the sensor.", class_member=True)
    
    specifications = TypedDict(default={
                                "Name"               : "QE12LP-S-MB-QED-D0",
                                "Absorber"           : "MB",
                                "Minimum Wavelength" : "266nm",
                                "Maximum Wavelength" : "2100nm",
                                "Total Spectral Range (inkl. Uncalibrated)" : "190 - 20000 nm",
                                "Typical Sensitivity"     : "60 V/J",
                                "Calibration Uncertainty" : "+- 3%",
                                "Repeatability"           : "<0.5%",   
                                "Max Pulse Energy 1064nm" : "3.9J",
                                "Max Pulse Energy 266nm"  : "0.81J",
                                "Noise Equivalent Energy" : "0.7uJ",
                                "Max Repetition Rate"     : "300Hz",
                                "Typical Rise Time"       : "550µsec",
                                "Max Pulse Width"         : "400µsec",
                                "Max Energy Density 1064nm" : "600mJ/cm^2 , 7ns, 10Hz",
                                "Max Energy Density 266nm"  : "500mJ/cm^2 , 7ns, 10Hz",
                                "Max Average Power w/o Heatsink"  : "5W",
                                "Max Average Power with Heatsink" : "10W",
                                "Max Power Density"         : "10W/cm^2 @ 5W",
                                "Dimensions w/o Heatsink"   : "50x50x14mm",
                                "Dimensions with Heatsink"  : "50x50x52.5mm",
                                "Weight w/o Heatsink"       : "120g",
                                "Weight with Heatsink"      : "187g",
                                "Aperture"                  : "25x25mm",
                                "Aperture Area"             : "6.25cm^2"
                            }, readonly=True, class_member=True,
                            doc="The specifications of the sensor.")
    


allowed_sensors = {
    QE25LP_S_MB.name : QE25LP_S_MB,
    QE12LP_S_MB_QED_D0.name : QE12LP_S_MB_QED_D0,
    None : QE25LP_S_MB
} 